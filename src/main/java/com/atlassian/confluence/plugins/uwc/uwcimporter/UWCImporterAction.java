package com.atlassian.confluence.plugins.uwc.uwcimporter;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.setup.settings.ConfluenceFlavour;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Action used by UWC Importer webitem.
 */
public class UWCImporterAction extends ConfluenceActionSupport {

	Logger log = Logger.getLogger(this.getClass());

    public boolean isPermitted() {
		return ConfluenceFlavour.selected().equals(ConfluenceFlavour.VANILLA);
	}

	public String execute() {
		//go straight to velocity template
		return SUCCESS; 
	}
	
	/* Properties helper methods
	 * XXX Having trouble loading the resources file via the plugin framework,
	 * so will directly load the properties if loading problem is detected 
	 */

	/** 
	 * gets the value of the given key from the resource file associated with this action.
	 * based on ConfluenceWebFragmentHelper.getI18nValue
	 */
	public String getText(String key) {
		I18NBean i18NBean = getI18n();
		String val = i18NBean.getText(key);
		if (key.equals(val)) {
			val = getTextDirectly(key);
		}
		return val;
	}

	public static final String PROPS_LOCATION = "resources";
	public static final String PROPS_FILE = "Webui.properties";

	/**
	 * gets value for given key by directly loading the properties file
	 * @param key
	 * @return value
	 */
	private String getTextDirectly(String key) {
		String jarseparator = "/"; 
		String path = jarseparator + PROPS_LOCATION + jarseparator + PROPS_FILE;
		Map properties = getProperties(path);
		return (String) properties.get(key);
	}
	/**
	 * gets properties from the given properties file located in a jar or null if could not load
	 * @param filename ex: "/resources/CustomizeAction.properties". If no directory,
	 * the file will be assumed to be in the same directory as the java file with the same name. 
	 * @param log pass in a log object for log messages or null if no logging is desired
	 * @return map of properties
	 */
	private Map getProperties(String filename) {

		try {
			InputStream in = this.getClass().getResourceAsStream(filename);
			Properties properties = new Properties();
			properties.load(in);
			return properties;
		} catch (Exception e) {
			if (log != null)
				log.error("Could not load props file: " + filename);
			e.printStackTrace();
		}
		return new HashMap();
	}
	
	/* Identify Confluence Version Helper */
	private static final int LAST_27_BUILDNUMBER = 1116; //reads: last 2.7 build number

    /**
     * @return true if the Confluence running is 2.7 or earlier; false if the Confluence is 2.8 or later
     */
	public boolean isEarlierConfluence() {
		String buildNumber = GeneralUtil.getBuildNumber();
		int build = LAST_27_BUILDNUMBER + 1;
        try
        {
            build = Integer.parseInt(buildNumber);
        }
        catch (NumberFormatException ex)
        {
            // probably a development version of Confluence so false will be returned
        }
		
		return build <= LAST_27_BUILDNUMBER;
	}

}
